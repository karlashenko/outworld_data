using System;
using Outworld.Data.Types;

namespace Outworld.Data
{
    [Serializable]
    public class PropertyData
    {
        public int Id;
        public PropertyType Type;
        public string NameKey;
        public string DescriptionKey;
        public float Min;
        public float Max;
    }
}