using System;
using System.Collections.Generic;

// ReSharper disable InconsistentNaming

namespace Outworld.Data
{
    [Serializable]
    public class I18nData
    {
        public int Id;
        public string Key;
        public List<I18nEffectData> Effects = new List<I18nEffectData>();
    }

    [Serializable]
    public class I18nEffectData
    {
        public int EffectId;
        public string Placeholder;
    }
}