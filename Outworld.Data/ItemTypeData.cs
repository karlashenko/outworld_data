using System;

namespace Outworld.Data
{
    [Serializable]
    public class ItemTypeData
    {
        public int Id;
        public string NameKey;
        public int Width;
        public int Height;
    }
}