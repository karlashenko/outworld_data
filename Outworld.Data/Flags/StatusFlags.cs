using System;
using Newtonsoft.Json;

namespace Outworld.Data.Flags
{
    [Flags]
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum StatusFlags
    {
        None = 0,
        Stun = 1 << 1,
        Slow = 1 << 2,
        MindControl = 1 << 3,
    }
}