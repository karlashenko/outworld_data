using System;
using Newtonsoft.Json;

namespace Outworld.Data.Flags
{
    [Flags]
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum ItemFlags
    {
        None,
        Stackable,
    }
}