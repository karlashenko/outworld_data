using System;
using Newtonsoft.Json;

namespace Outworld.Data.Flags
{
    [Flags]
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum BehaviourFlags
    {
        None = 0,
        BreaksOnTakeDamage = 1 << 1,
        BreaksOnDealDamage = 1 << 2,
        BreaksOnDeath = 1 << 3,
        Dispellable = 1 << 4,
    }
}