using System;
using System.Collections.Generic;
using Outworld.Data.Flags;
using Outworld.Data.Types;

namespace Outworld.Data
{
    [Serializable]
    public class EffectData
    {
        public int Id;
        public string Name;
        public EffectType Type;
        public string OriginParticlesPrefab;
        public string TargetParticlesPrefab;
        public bool IsImmediate;

        public DamageType DamageType;
        public string DamageFormula;

        public int ApplyBehaviourStackCount;
        public int ApplyBehaviourBehaviourId;

        public BehaviourFlags RemoveBehaviourFlags;
        public int RemoveBehaviourBehaviourId;

        public int ProjectileId;
        public int ProjectileEffectId;

        public int SearchAreaTargetLimit;
        public float SearchAreaRadius;
        public int SearchAreaEffectId;
        public int SearchAreaValidatorId;

        public List<int> Effects = new List<int>();
    }
}