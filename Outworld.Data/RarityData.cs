using System;

namespace Outworld.Data
{
    [Serializable]
    public class RarityData
    {
        public int Id;
        public string NameKey;
        public string Color;
    }
}