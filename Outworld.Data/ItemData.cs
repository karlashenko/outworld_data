﻿using System;
using Outworld.Data.Flags;

namespace Outworld.Data
{
    [Serializable]
    public class ItemData
    {
        public int Id;
        public string NameKey;
        public string DescriptionKey;
        public string Icon;
        public int ItemTypeId;
        public int RarityId;
        public ItemFlags ItemFlags;
    }
}