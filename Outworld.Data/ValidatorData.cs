using System;
using System.Collections.Generic;
using Outworld.Data.Flags;
using Outworld.Data.Types;

namespace Outworld.Data
{
    [Serializable]
    public class ValidatorData
    {
        public int Id;
        public string Name;
        public ValidatorType Type;
        public ComparatorType Comparator;
        public float Value;

        public int UnitId;
        public int BehaviourId;

        public StatusFlags StatusFlags;
        public UnitFlags UnitFlags;

        public List<int> Validators = new List<int>();
    }
}