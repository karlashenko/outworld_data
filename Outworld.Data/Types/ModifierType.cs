namespace Outworld.Data.Types
{
    public enum ModifierType
    {
        Add,
        Multiply
    }
}