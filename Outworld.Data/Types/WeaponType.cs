namespace Outworld.Data.Types
{
    public enum WeaponType
    {
        None,
        Hitscan,
        Projectile,
        Beam
    }
}