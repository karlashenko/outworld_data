namespace Outworld.Data.Types
{
    public enum ValidatorType
    {
        CasterHealthFraction,
        TargetHealthFraction,
        TargetIsAlly,
        TargetIsEnemy,
    }
}