namespace Outworld.Data.Types
{
    public enum EffectType
    {
        None,
        Damage,
        Set,
        SearchArea,
        Projectile,
    }
}