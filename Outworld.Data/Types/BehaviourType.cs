namespace Outworld.Data.Types
{
    public enum BehaviourType
    {
        None,
        Buff,
        ModifyProperties,
        OnTakeDamage,
        OnDealDamage,
        OnKill,
    }
}