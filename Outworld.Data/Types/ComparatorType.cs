namespace Outworld.Data.Types
{
    public enum ComparatorType
    {
        None,
        Greater,
        Equal,
        Less,
        GreaterOrEqual,
        LessOrEqual,
    }
}