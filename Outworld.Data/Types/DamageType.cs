namespace Outworld.Data.Types
{
    public enum DamageType
    {
        None,
        True,
        Kinetic,
        Energy,
        Explosion,
        Biological,
        Psychic,
        Radiation,
        Corrosive,
    }
}