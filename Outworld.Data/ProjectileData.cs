using System;

namespace Outworld.Data
{
    [Serializable]
    public class ProjectileData
    {
        public int Id;
        public string Name;
        public string Prefab;
        public string ImpactPrefab;
        public float Speed;
        public float Lifetime;
        public bool DestroyOnCollision;
        public bool RicochetOnCollision;
        public bool IsHoming;
        public float HomingRadius;
        public float HomingSpeed;
    }
}