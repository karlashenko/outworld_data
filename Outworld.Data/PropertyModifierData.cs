using System;
using Outworld.Data.Types;

namespace Outworld.Data
{
    [Serializable]
    public class PropertyModifierData
    {
        public int Id;
        public int PropertyId;
        public ModifierType Type;
        public float Amount;
    }
}