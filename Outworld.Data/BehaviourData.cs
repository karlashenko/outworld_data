using System;
using System.Collections.Generic;
using Outworld.Data.Flags;
using Outworld.Data.Types;

namespace Outworld.Data
{
    [Serializable]
    public class BehaviourData
    {
        public int Id;

        public string NameKey;
        public string DescriptionKey;

        public string Icon;
        public BehaviourType Type;
        public float Duration;
        public int StackCount;
        public bool IsPositive;
        public BehaviourFlags Flags;
        public StatusFlags StatusFlags;

        public int OnKillEffectId;
        public int OnDeathEffectId;

        public int OnTakeDamageEffectId;
        public int OnDealDamageEffectId;

        public float AuraRadius;
        public int AuraBehaviourId;
        public int AuraValidatorId;

        public float BuffTickPeriod;
        public int BuffTickEffectId;
        public int BuffApplyEffectId;
        public int BuffRemoveEffectId;
        public int BuffExpireEffectId;

        public List<int> PropertyModifiers = new List<int>();
        public List<int> Behaviours = new List<int>();
    }
}