using System;
using Outworld.Data.Types;

namespace Outworld.Data
{
    [Serializable]
    public class WeaponData
    {
        public int Id;
        public string Name;
        public string Prefab;
        public WeaponType Type;
        public float Range;
        public float CooldownTime;
        public float PreparationTime;
        public bool IsCharging;
        public float ChargeDurationSeconds;
        public int EffectId;
        public int ProjectileId;

        public string OriginParticlesPrefab;
        public string TargetParticlesPrefab;

        public string BeamPrefab;
        public int BeamReflectionCount;

        public int ShotSpreadCount;
        public float ShotSpreadDegrees;
        public int ShotBarrageCount;
        public float ShotBarrageInterval;
    }
}