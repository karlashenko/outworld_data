using System;
using System.Collections.Generic;

// ReSharper disable InconsistentNaming

namespace Outworld.Data
{
    [Serializable]
    public class Dataset
    {
        public List<ItemData> Items = new();
        public List<ItemTypeData> ItemTypes = new();
        public List<WeaponData> Weapon = new();
        public List<ProjectileData> Projectiles = new();
        public List<PropertyData> Properties = new();
        public List<EffectData> Effects = new();
        public List<BehaviourData> Behaviours = new();
        public List<ValidatorData> Validators = new();
        public List<I18nData> I18n = new();
        public List<PropertyModifierData> PropertyModifiers = new();
    }
}